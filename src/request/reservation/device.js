import service from '@/request/base'

export default {
  save (body) {
    return service.post('huipmaa/maaapi/maa/device/save', body)
  },
  delete (body) {
    return service.post('huipmaa/maaapi/maa/device/delete', body)
  },
  load (body) {
    return service.post('huipmaa/maaapi/maa/device/load', body)
  },
  list (body) {
    return service.post('huipmaa/maaapi/maa/device/list', body)
  },
  find (body, pagination) {
    return service.post('huipmaa/maaapi/maa/device/find', service.paginationBody(body, pagination))
  }
}
