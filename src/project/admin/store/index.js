import UserUserStore from 'admin/store/modules/user/user'

const state = {
}

const mutations = {
}

const actions = {
}

const getters = {
}

export default {
  namespaced: true,
  state,
  mutations,
  actions,
  getters,
  modules: {
    'UserUserStore': UserUserStore
  }
}
