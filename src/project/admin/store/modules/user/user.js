import _ from 'lodash'

const state = {
  pagination: {
    sort: null,
    order: null,
    size: 20,
    total: 0,
    current: 1,
    layout: 'total, sizes, prev, pager, next, jumper',
    sizes: [10, 20, 30]
  }
}

const mutations = {
  setPagination (state, pagination) {
    pagination = pagination || {}
    state.pagination = _.defaultsDeep(pagination, state.pagination)
  }
}

const actions = {
  updatePagination ({commit, state}, payload) {
    return new Promise(function (resolve, reject) {
      commit('setPagination', payload)
      resolve()
    })
  }
}

const getters = {
  pagination: function (state) {
    return state.pagination
  }
}

export default {
  namespaced: true,
  state,
  mutations,
  actions,
  getters
}
