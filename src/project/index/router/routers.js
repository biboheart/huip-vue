import index from 'index/pages/index'
const Wrap = { template: '<router-view></router-view>' }

const routers = [{
  path: '/',
  name: 'index',
  text: '首页',
  redirect: '/home',
  component: index,
  children: [{
    path: '/home',
    name: 'home',
    text: '导航',
    menu: false,
    component: function (resolve) {
      require(['index/pages/home'], resolve)
    }
  }, {
    path: '/guide',
    name: 'guide',
    text: '导医台',
    menu: true,
    // icon: 'fas fa-hand-holding-heart',
    component: Wrap,
    hasChildren: true,
    children: [{
      path: '/guide/patient',
      name: 'guide_patient',
      text: '患者录入',
      menu: true,
      component: function (resolve) {
        require(['index/pages/patient/add'], resolve)
      }
    }]
  }, {
    path: '/charge',
    name: 'charge',
    text: '挂号收费',
    menu: true,
    // icon: 'fas fa-hand-holding-usd',
    component: Wrap,
    hasChildren: true,
    children: [{
      path: '/charge/register',
      name: 'charge_register',
      text: '挂号',
      menu: true,
      component: function (resolve) {
        require(['index/pages/charge/register'], resolve)
      }
    }, {
      path: '/charge/fees',
      name: 'charge_fees',
      text: '收费',
      menu: true,
      component: function (resolve) {
        require(['index/pages/charge/fees'], resolve)
      }
    }]
  }, {
    path: '/reservation',
    name: 'reservation',
    text: '预约中心',
    menu: true,
    // icon: 'fas fa-hand-holding-usd',
    component: Wrap,
    hasChildren: true,
    children: [{
      path: '/reservation/inspect',
      name: 'reservation_inspect',
      text: '预约检查',
      menu: true,
      component: function (resolve) {
        require(['index/pages/reservation/inspect'], resolve)
      }
    }, {
      path: '/reservation/reservation/:pat',
      name: 'reservation_reservation',
      text: '预约',
      menu: true,
      component: function (resolve) {
        require(['index/pages/reservation/reservation'], resolve)
      }
    }]
  }]
}]

export default routers
