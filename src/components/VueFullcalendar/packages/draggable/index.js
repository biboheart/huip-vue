import VueDraggable from './src/draggable'

/* istanbul ignore next */
VueDraggable.install = function (Vue) {
  Vue.component(VueDraggable.name, VueDraggable)
}

export default VueDraggable
