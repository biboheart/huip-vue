import VueFullcalendar from './src/calendar'

/* istanbul ignore next */
VueFullcalendar.install = function (Vue) {
  Vue.component(VueFullcalendar.name, VueFullcalendar)
}

export default VueFullcalendar
