import Mock from 'mockjs'
import moment from 'moment'
import RequestUtil from '@/utils/request'

const List = Mock.mock({
  'list|30': [{
    'id|+1': 1,
    'pat|1-20': 20,
    name: '@csentence(3, 5)',
    'done|1-3': 3,
    createTime: moment().valueOf(),
    updateTime: moment().valueOf()
  }]
}).list

export default {
  list: config => {
    const { pat } = RequestUtil.param2Obj(config.url)

    let mockList = List.filter(item => {
      if (pat && item.pat !== parseInt(pat)) return false
      return true
    })

    return {
      code: 0,
      message: 'success',
      result: mockList
    }
  }
}
