import Mock from 'mockjs'
import moment from 'moment'
import RequestUtil from '@/utils/request'

const List = Mock.mock({
  'list|20': [{
    'id|+1': 1,
    name: '@cname',
    'done|1-3': 3,
    createTime: moment().valueOf(),
    updateTime: moment().valueOf()
  }]
}).list

export default {
  list: config => {
    const { done } = RequestUtil.param2Obj(config.url)

    let mockList = List.filter(item => {
      if (done && item.done !== parseInt(done)) return false
      return true
    })

    return {
      code: 0,
      message: 'success',
      result: mockList
    }
  }
}
