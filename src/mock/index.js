import Mock from 'mockjs'
import UserApi from './user/users'
import CommentApi from './comments/comment'
import ReplyApi from './comments/reply'
import ReservationPatientApi from './reservation/patient'
import ReservationProjectApi from './reservation/project'

// user
Mock.mock(/\/user\/user\/list/, 'get', UserApi.list)
// comments
Mock.mock(/\/comments\/comment\/list/, 'get', CommentApi.list)
Mock.mock(/\/comments\/reply\/list/, 'get', ReplyApi.list)
// reservation
Mock.mock(/\/reservation\/patient\/list/, 'get', ReservationPatientApi.list)
Mock.mock(/\/reservation\/project\/list/, 'get', ReservationProjectApi.list)

export default Mock
