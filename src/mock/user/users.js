import Mock from 'mockjs'
import moment from 'moment'
import RequestUtil from '@/utils/request'

const List = Mock.mock({
  'list|100': [{
    'id|+1': 1,
    name: Mock.Random.cname(),
    phone: null,
    birthday: null,
    createTime: moment().valueOf(),
    updateTime: moment().valueOf()
  }]
}).list

export default {
  list: config => {
    const { name, pageOffset = 1, pageSize = 20, sort } = RequestUtil.param2Obj(config.url)

    let mockList = List.filter(item => {
      if (name && item.title.indexOf(name) < 0) return false
      return true
    })

    if (sort === '-id') {
      mockList = mockList.reverse()
    }

    const pageList = mockList.filter((item, index) => index < pageSize * pageOffset && index >= pageSize * (pageOffset - 1))

    return {
      code: 0,
      message: 'success',
      result: {
        first: pageOffset === 1,
        last: pageOffset >= List.length / pageSize,
        totalPages: List.length / pageSize,
        totalElements: List.length,
        number: pageOffset - 1,
        size: pageList.length,
        numberOfElements: (pageSize * (pageOffset - 1)) + pageList.length,
        content: pageList
      }
    }
  }
}
