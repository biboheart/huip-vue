import ReservationDeviceEdit from './src/device-edit'
import ReservationProjectEdit from './src/project-edit'
import ReservationInspectEdit from './src/inspect-edit'
import ReservationInspectTypeEdit from './src/inspect-type-edit'

const components = [
  ReservationDeviceEdit,
  ReservationProjectEdit,
  ReservationInspectEdit,
  ReservationInspectTypeEdit
]

const install = function (Vue) {
  components.map(component => {
    Vue.component(component.name, component)
  })
}

if (typeof window !== 'undefined' && window.Vue) {
  install(window.Vue)
}

export default {
  version: '1.0.1',
  name: 'HuipReservation',
  install,
  ReservationDeviceEdit,
  ReservationProjectEdit,
  ReservationInspectEdit,
  ReservationInspectTypeEdit
}
